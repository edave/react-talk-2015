var path = require("path");
var cwd = process.cwd();

var SRC_JS_REGEX = /\/src\/js\/.*?\.js$/;

module.exports = {
    cache: false,
    quiet: false,
    noInfo: true,
    debug: false,
    entry: path.resolve(__dirname, 'src/js/index.js'),
    devtool: "source-map",
    output: {
        path: path.join(__dirname, "public"),
        filename: "app.js"
    },
    resolve: {
        alias: {
            "actions": path.join(cwd, "src/js/actions"),
            "constants": path.join(cwd, "src/js/constants"),
            "components": path.join(cwd, "src/js/components"),
            "dispatcher": path.join(cwd, "src/js/dispatcher"),
            "stores": path.join(cwd, "src/js/stores"),
            "less": path.join(cwd, "src/less")
        }
    },
    module: {
        loaders: [
            {test: SRC_JS_REGEX, loader: "babel-loader"},
            {test: /\.less$/, loader: "style!css!less"}
        ]
    }
};