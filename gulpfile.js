var gulp = require('gulp');
var gutil = require("gulp-util");
var gls = require('gulp-live-server');
var runSequence = require('run-sequence');
var webpack = require("webpack");

var paths = {
    script: 'src/js/**/*.js',
    less: 'src/less/**/*.less',
    main: 'src/js/index.js',
    public: 'public/**/*'
};

var webpackCompiler = webpack(require("./webpack.config"));

gulp.task("copy", function() {
    gulp.src("./src/js/components/**").pipe(gulp.dest('./public/components'))
});

gulp.task("script", function(done) {
    webpackCompiler.run(function(err, stats) {
        if (err) throw new gutil.PluginError("webpack:build-dev", err);
        gutil.log("[webpack:build-dev]", stats.toString({
            colors: true
        }));
        done();
    });
});

gulp.task('watch', ['default'], function() {
    gulp.watch([paths.script, paths.less], ['script'])
});

gulp.task('serve', function() {
  var server = gls.static(['public']);
  server.start();

  gulp.watch(paths.public, server.notify);
});

gulp.task('default', function() {
  runSequence('script', 'copy', 'serve');
});