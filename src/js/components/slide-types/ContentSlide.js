import React from "react";
import Slide from "components/Slide";
import classnames from "classnames";

export default React.createClass({

  displayName: "ContentSlide",

  _getTitle() {
    if (this.props.title) {
      return <h2>{this.props.title}</h2>;
    }
  },

  render() {
    var title = this._getTitle();
    var classes = classnames("content", this.props.className);
    return (
      <Slide className={classes}>
        {title}
        <div className="children">{this.props.children}</div>
      </Slide>
    );
  }
});