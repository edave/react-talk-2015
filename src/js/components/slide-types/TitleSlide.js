import React from "react";
import Slide from "components/Slide";
import classnames from "classnames";

export default React.createClass({

  displayName: "TitleSlide",

  render() {
    var classes = classnames(this.props.className, "title");
    return (
      <Slide className={classes}>
        <h1>{this.props.title}</h1>
        {this.props.children}
      </Slide>
    );
  }
});