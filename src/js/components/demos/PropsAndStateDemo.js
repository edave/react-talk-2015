import React from "react";
import SimplePropsDemo from "components/demos/SimplePropsDemo";

export default React.createClass({

  displayName: "PropsAndStateDemo",

  getInitialState() {
    return {
      text: "Follow @edave"
    }
  },

  _onInput(e) {
    this.setState({
      text: e.target.value
    });
  },

  render() {
    return (
      <div className="propsDemo">
        <h4>Component with state</h4>
        <input type="text" value={this.state.text} onInput={this._onInput} />
        <SimplePropsDemo text={this.state.text} />
      </div>
    );
  }
});