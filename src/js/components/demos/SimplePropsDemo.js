import React from "react";

export default React.createClass({

  displayName: "SimplePropDemo",

  render() {
    return (
      <div className="propsDemo">
        <h4>Component with props</h4>
        <span>Value: {this.props.text}</span>
      </div>
    );
  }
});