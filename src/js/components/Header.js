import React from "react";
import Controls from "components/Controls";

export default React.createClass({

  displayName: "Header",

  render() {
    return (
      <header className="highlightable-component">
        <a href="https://www.twitter.com/edave">@edave</a>
      </header>
    );
  }
});