import React from "react";
import Controls from "components/Controls";

export default React.createClass({

  displayName: "Footer",

  render() {
    return (
      <footer className="highlightable-component">
        <span>{this.props.slide} / {this.props.slideCount}</span>
        <a href="https://bitbucket.org/edave/react-talk-2015">https://bitbucket.org/edave/react-talk-2015</a>
        <Controls {...this.props} />
      </footer>
    );
  }
});