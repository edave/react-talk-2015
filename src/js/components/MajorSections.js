import SelectableList from "components/SelectableList";
import React from "react";

export default React.createClass({

  displayName: "Major Sections",

  getDefaultProps() {
    return {
      sections: ["Why use React?", "Flux", "Hints and Tips"],
      selected: "Why use React?"
    }
  },

  render() {
    return <SelectableList {...this.props} />
  }
});