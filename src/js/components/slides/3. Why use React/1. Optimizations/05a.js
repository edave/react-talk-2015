import ContentSlide from "components/slide-types/ContentSlide";
import React from "react";

export default React.createClass({

  displayName: "Reflow example",

  render() {
    return (
      <ContentSlide title="Reflow" {...this.props}>
        <iframe width="640" height="480" src="https://www.youtube.com/embed/ZTnIxIA5KGw?rel=0&amp;controls=0&amp;showinfo=0&autoplay=1" frameborder="0" allowfullscreen></iframe>
      </ContentSlide>
    );
  }
});