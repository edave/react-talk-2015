import ContentSlide from "components/slide-types/ContentSlide";
import ReactSections from "components/ReactSections";
import React from "react";

export default React.createClass({

  displayName: "Why use React? - The Virtual DOM",

  render() {
    return (
      <ContentSlide title="The Virtual DOM" {...this.props} />
    );
  }
});