import React from "react";
import ContentSlide from "components/slide-types/ContentSlide";
import CodeSnippet from "components/CodeSnippet"

var code = `React.createClass({
  _onClick(e) {
    // Context is auto-bound to Component
    this.checkState();
  },
  checkState() {
    alert("Hello");
  },
  render() {
    return <a onClick={this._onClick}>Click here</a>;
  }
});`;

export default React.createClass({

  displayName: "Auto context binding",

  _onClick() {
    this.checkState();
  },

  checkState() {
    alert("Hello");
  },

  render() {
    return (
      <ContentSlide title="Auto context binding" {...this.props}>
        <p>No need for this._onClick.bind(this)</p>
        <CodeSnippet onClick={this._onClick}>{code}</CodeSnippet>
      </ContentSlide>
    );
  }
});