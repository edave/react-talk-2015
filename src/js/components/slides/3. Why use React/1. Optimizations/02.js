import ContentSlide from "components/slide-types/ContentSlide";
import ReactSections from "components/ReactSections";
import React from "react";

export default React.createClass({

  displayName: "Why use React? - Optimizations baked in",

  render() {
    return (
      <ContentSlide title="Why use React?" {...this.props}>
        <ReactSections />
      </ContentSlide>
    );
  }
});