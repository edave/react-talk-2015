import ContentSlide from "components/slide-types/ContentSlide";
import React from "react";

export default React.createClass({

  displayName: "DOM diff",

  render() {
    return (
      <ContentSlide title="Virtual DOM -> DOM Diff" {...this.props}>
        <p>Demo</p>
      </ContentSlide>
    );
  }
});