import ContentSlide from "components/slide-types/ContentSlide";
import React from "react";

export default React.createClass({

  displayName: "The DOM",

  render() {
    return (
      <ContentSlide title="The DOM" {...this.props}>
        <p>...is like a packed carpark</p>
        <img src="/img/640px-Parking_lot_at_HAA_Kobe.jpg" width="485" />
      </ContentSlide>
    );
  }
});