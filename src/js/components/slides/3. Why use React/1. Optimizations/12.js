import React from "react";
import ContentSlide from "components/slide-types/ContentSlide";
import CodeSnippet from "components/CodeSnippet"

var code = `<footer data-reactid=".0.2">
  <span data-reactid=".0.2.0">
    <span data-reactid=".0.2.0.0">22</span>
    <span data-reactid=".0.2.0.1"> / </span>
    <span data-reactid=".0.2.0.2">50</span>
  </span>
</footer>
`;

export default React.createClass({

  displayName: "Element ID encoding",

  render() {
    return (
      <ContentSlide title="Element ID encoding" {...this.props}>
        <p>Element IDs encode hierachy. No Virtual DOM traversal needed!</p>
        <CodeSnippet>{code}</CodeSnippet>
      </ContentSlide>
    );
  }
});