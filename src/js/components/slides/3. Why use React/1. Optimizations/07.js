import ContentSlide from "components/slide-types/ContentSlide";
import React from "react";

export default React.createClass({

  displayName: "DOM Diffing",

  render() {
    return (
      <ContentSlide title="Virtual DOM -> DOM Diff" {...this.props}>
        <p>The least number of DOM updates as possible</p>
        <p>
          <a href="http://calendar.perfplanet.com/2013/diff/">Read more here</a>
        </p>
      </ContentSlide>
    );
  }
});