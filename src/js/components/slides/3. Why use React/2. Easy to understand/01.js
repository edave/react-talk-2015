import ContentSlide from "components/slide-types/ContentSlide";
import ReactSections from "components/ReactSections";
import React from "react";

export default React.createClass({

  displayName: "It's easy to understand",

  render() {
    return (
      <ContentSlide {...this.props}>
        <ReactSections selected="It's easy to understand" />
      </ContentSlide>
    );
  }
});