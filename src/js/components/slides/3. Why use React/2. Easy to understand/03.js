import React from "react";
import ContentSlide from "components/slide-types/ContentSlide";
import CodeSnippet from "components/CodeSnippet"

var code = `React.createClass({
  render() {
    return (
      <ContentSlide title="Declarative">
        <MyCodeSnippet>var foo = "bar"</MyCodeSnippet>
      </ContentSlide>
    );
  }
})`;


export default React.createClass({

  displayName: "Declarative",

  render() {
    return (
      <ContentSlide title="Declarative" {...this.props}>
        <CodeSnippet>{code}</CodeSnippet>
      </ContentSlide>
    );
  }
});