import React from "react";
import ContentSlide from "components/slide-types/ContentSlide";
import CodeSnippet from "components/CodeSnippet"
import PropsAndStateDemo from "components/demos/PropsAndStateDemo"
import Split from "components/Split"

var code = `var PropsDemo = React.createClass({
  render() {
    return (
      <span>Value: {this.props.text}</span>
    );
  }
});`;

export default React.createClass({

  displayName: "Props",

  render() {
    return (
      <ContentSlide title="Props" {...this.props}>
        <Split>
          <CodeSnippet>{code}</CodeSnippet>
          <PropsAndStateDemo />
        </Split>
      </ContentSlide>
    );
  }
});