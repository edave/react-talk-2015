import TitleSlide from "components/slide-types/TitleSlide";
import React from "react";

export default React.createClass({

  displayName: "React primitives",

  render() {
    return (
      <TitleSlide title="React primitives" {...this.props} />
    );
  }
});