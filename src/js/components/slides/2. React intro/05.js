import React from "react";
import ContentSlide from "components/slide-types/ContentSlide";
import CodeSnippet from "components/CodeSnippet"

var jsx = `React.createClass({
  render() {
    return <div>foo</div>;
  }
});`;

var code = `React.createClass({
  render() {
    return React.createElement('div', null, 'foo');
  }
});`;


export default React.createClass({

  displayName: "JSX",

  render() {
    return (
      <ContentSlide title="JSX" {...this.props}>
        <CodeSnippet>{jsx}</CodeSnippet>
        <p>when <a href="https://babeljs.io/">compiled</a> becomes</p>
        <CodeSnippet>{code}</CodeSnippet>
      </ContentSlide>
    );
  }
});