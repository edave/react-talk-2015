import React from "react";
import ContentSlide from "components/slide-types/ContentSlide";
import CodeSnippet from "components/CodeSnippet"

var code = `React.createClass({
  render() {
    return <div>foo</div>;
  }
});`;

export default React.createClass({

  displayName: "Components",

  render() {
    return (
      <ContentSlide title="Components" {...this.props}>
        <p>Reusable, composable views</p>
        <CodeSnippet>{code}</CodeSnippet>
      </ContentSlide>
    );
  }
});
