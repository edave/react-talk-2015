import React from "react";
import ContentSlide from "components/slide-types/ContentSlide";
import CodeSnippet from "components/CodeSnippet"

var code = `React.createClass({
  render() {
    return <div>foo</div>;
  }
});`;


export default React.createClass({

  displayName: "JSX",

  render() {
    return (
      <ContentSlide title="JSX" {...this.props}>
        <CodeSnippet>{code}</CodeSnippet>
        <img src="/img/donotwant.png" width="500" height="318" />
      </ContentSlide>
    );
  }
});