import React from "react";
import ContentSlide from "components/slide-types/ContentSlide";

export default React.createClass({

  displayName: "Flux - Stores",

  render() {
    return (
      <ContentSlide title="Stores" {...this.props}>
        <img src="/img/flux-simple-f8-diagram-1300w.png" width="650" />
        <p>Houses data and logic</p>
        <p>Emit events when data changes</p>
      </ContentSlide>
    );
  }
});