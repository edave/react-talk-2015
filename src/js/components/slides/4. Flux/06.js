import React from "react";
import ContentSlide from "components/slide-types/ContentSlide";

export default React.createClass({

  displayName: "Flux - Stores",

  render() {
    return (
      <ContentSlide title="Views" {...this.props}>
        <h3>(React Components)</h3>
        <img src="/img/flux-simple-f8-diagram-with-client-action-1300w.png" width="650" />
        <p>Efficiently display data from Stores</p>
        <p>Call Actions from UI events</p>
      </ContentSlide>
    );
  }
});