import ContentSlide from "components/slide-types/ContentSlide";
import MajorSections from "components/MajorSections";
import React from "react";

export default React.createClass({

  displayName: "Contents",

  render() {
    return (
      <ContentSlide {...this.props}>
        <svg className="atlassian-logo" dangerouslySetInnerHTML={{__html: "<use xlink:href='#atlassian-logo'>"}}></svg>
        <p><a href="https://www.atlassian.com/company/careers">We're hiring</a></p>
      </ContentSlide>
    );
  }
});