import ContentSlide from "components/slide-types/ContentSlide";
import MajorSections from "components/MajorSections";
import React from "react";

export default React.createClass({

  displayName: "Performance - Immutable.js",

  render() {
    return (
      <ContentSlide {...this.props}>
        <svg className='immutable-logo' dangerouslySetInnerHTML={{__html: "<use xlink:href='#immutable-logo'></use>"}} />
        <ul>
          <li>Look at the code of this presentation</li>
          <li>Allows for deep comparison</li>
          <li>Change events only when something changes</li>
          <li>Less change events = less renders</li>
          <li><a href="https://facebook.github.io/react/docs/advanced-performance.html#immutable-js-to-the-rescue">Read more here</a></li>
        </ul>
      </ContentSlide>
    );
  }
});