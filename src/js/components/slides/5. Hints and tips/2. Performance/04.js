import BulletListSlide from "components/slide-types/BulletListSlide";
import React from "react";

export default React.createClass({

  displayName: "Performance - Immutable.js",

  render() {
    return (
      <BulletListSlide title="PureRenderMixin" {...this.props}>
        <p>Only re-render if props/state have changed</p>
        <p>Shallow check only</p>
        <p>Simple components are faster</p>
        <p>Send only the data required</p>
        <p><a href="https://facebook.github.io/react/docs/pure-render-mixin.html">Read more here</a></p>
      </BulletListSlide>
    );
  }
});