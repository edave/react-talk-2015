import ContentSlide from "components/slide-types/ContentSlide";
import MajorSections from "components/MajorSections";
import React from "react";

export default React.createClass({

  displayName: "Performance",

  render() {
    return (
      <ContentSlide title="React Performance Tools" {...this.props}>
        <p>Meta demo!</p>
      </ContentSlide>
    );
  }
});