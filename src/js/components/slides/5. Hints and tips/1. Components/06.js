import React from "react";
import ContentSlide from "components/slide-types/ContentSlide";
import CodeSnippet from "components/CodeSnippet"

var code = `React.createClass({
  _getOptionalElement() {
    if (this.props.foo) {
      return <p>foo</p>;
    }
  },
  render() {
    var element = this._getOptionalElement();
    return (
      <div>
        {element}
        <p>bar</p>
      </div>
    );
  }
});`;

export default React.createClass({

  displayName: "Use getters over ternaries 2",

  render() {
    return (
      <ContentSlide title="Use getters over ternaries" {...this.props}>
        <CodeSnippet>{code}</CodeSnippet>
      </ContentSlide>
    );
  }
});