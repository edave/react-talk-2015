import React from "react";
import ContentSlide from "components/slide-types/ContentSlide";
import CodeSnippet from "components/CodeSnippet"

var code = `React.createClass({
  onChange() {
    this.setState(Store.getAll());
  },
  componentDidMount() {
    Store.on("change", this.onChange);
  },
  componentWillUnmount() {
    Store.off("change", this.onChange);
  },
  render() {
    return (
      <div>foo</div>
    );
  }
});`;

export default React.createClass({

  displayName: "Always unbind external event listeners",

  render() {
    return (
      <ContentSlide title="Always unbind external event listeners" {...this.props}>
        <CodeSnippet>{code}</CodeSnippet>
      </ContentSlide>
    );
  }
});