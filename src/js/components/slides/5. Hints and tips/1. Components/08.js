import React from "react";
import ContentSlide from "components/slide-types/ContentSlide";
import CodeSnippet from "components/CodeSnippet"

var code = `React.createClass({
  _getExpensiveComponent() {
    if (this.props.expensiveThingVisible) {
      return <ExpensiveComponent />;
    }
  },
  render() {
    var component = this._getExpensiveComponent();
    return (
      <div>
        {component}
        <p>body</p>
      </div>
    );
  }
});`;

export default React.createClass({

  displayName: "Don't render unused Components",

  render() {
    return (
      <ContentSlide title="Don't render unused Components" {...this.props}>
        <p>Don't hide with CSS. Remove it entirely.</p>
        <CodeSnippet>{code}</CodeSnippet>
      </ContentSlide>
    );
  }
});