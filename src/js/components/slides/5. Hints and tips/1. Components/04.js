import React from "react";
import ContentSlide from "components/slide-types/ContentSlide";
import CodeSnippet from "components/CodeSnippet"

export default React.createClass({

  displayName: "Composing Components",

  render() {
    return (
      <ContentSlide title="Composing Components" {...this.props}>
        <img src="/img/settings-panel.png" height="500" />
      </ContentSlide>
    );
  }
});