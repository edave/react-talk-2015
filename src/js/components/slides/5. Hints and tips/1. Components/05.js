import React from "react";
import ContentSlide from "components/slide-types/ContentSlide";
import CodeSnippet from "components/CodeSnippet"

var code = `React.createClass({
  render() {
    return (
      <div>
        {this.props.foo ? <p>foo</p> : ''}
        <p>bar</p>
      </div>
    );
  }
});`;

export default React.createClass({

  displayName: "Use getters over ternaries 1",

  render() {
    return (
      <ContentSlide title="Use getters over ternaries" {...this.props}>
        <CodeSnippet>{code}</CodeSnippet>
      </ContentSlide>
    );
  }
});