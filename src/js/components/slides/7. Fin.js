import TitleSlide from "components/slide-types/TitleSlide";
import React from "react";

export default React.createClass({

  displayName: "Fin",

  render() {
    return (
      <TitleSlide title="Thank you" {...this.props}>
        <p>Questions?</p>
      </TitleSlide>
    );
  }
});