import ContentSlide from "components/slide-types/ContentSlide";
import React from "react";

export default React.createClass({

  displayName: "Javascript History",

  render() {
    return (
      <ContentSlide title="Javascript history" {...this.props}>
        <h3>Elightenment</h3>
        <ul>
          <li>2013: React, Flux, Immutable.js, etc</li>
        </ul>
      </ContentSlide>
    );
  }
});