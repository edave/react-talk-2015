import TitleSlide from "components/slide-types/TitleSlide";
import React from "react";

export default React.createClass({

  displayName: "Title Slide",

  render() {
    return (
      <TitleSlide title="React" className="react-title" {...this.props}>
        <svg className="react-logo" dangerouslySetInnerHTML={{__html: "<use xlink:href='#react-logo'>"}}></svg>
      </TitleSlide>
    );
  }
});