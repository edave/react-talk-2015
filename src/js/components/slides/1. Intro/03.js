import BulletListSlide from "components/slide-types/BulletListSlide";
import Split from "components/Split";
import React from "react";

export default React.createClass({

  displayName: "About Dave",

  render() {
    return (
      <BulletListSlide title="About this presentation" {...this.props}>
        <p>Written in React</p>
        <p>All examples are in ES6</p>
      </BulletListSlide>
    );
  }
});