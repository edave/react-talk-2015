import ContentSlide from "components/slide-types/ContentSlide";
import Split from "components/Split";
import React from "react";

export default React.createClass({

  displayName: "About Dave",

  render() {
    return (
      <ContentSlide title="Dave Elkan" {...this.props}>
        <Split>
          <img src="/img/edave.jpg" width="256" height="256" />
          <ul>
            <li>JIRA in Sydney</li>
            <li>Atlassian Marketplace</li>
            <li>HipChat</li>
            <li>Find me on twitter: <a href="https://twitter.com/edave">@edave</a></li>
          </ul>
        </Split>
      </ContentSlide>
    );
  }
});