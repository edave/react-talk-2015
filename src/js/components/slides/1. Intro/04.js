import ContentSlide from "components/slide-types/ContentSlide";
import React from "react";

export default React.createClass({

  displayName: "Javascript History",

  render() {
    return (
      <ContentSlide title="Javascript history" {...this.props}>
        <h3>Awakening</h3>
        <ul>
          <li>1995: document.write</li>
          <li>1998: Ajax</li>
          <li>2006: jQuery</li>
          <li>2010: Backbone</li>
        </ul>
      </ContentSlide>
    );
  }
});