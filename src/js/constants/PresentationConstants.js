import keyMirror from "key-mirror";

export default {
  ActionTypes: keyMirror({
    PREVIOUS_SLIDE: null,
    NEXT_SLIDE: null,
    TOGGLE_CONTRAST: null,
    TOGGLE_HIGHLIGHT_COMPONENTS: null
  })
};