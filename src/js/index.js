import React from "react/addons"
import "less/index.less";
import Presentation from "components/Presentation.js";
import PresentationStore from "stores/PresentationStore.js"

var SLIDE_REGEX = /^#([0-9]+)$/;

function onHashChange() {
  var slide = window.location.hash.match(SLIDE_REGEX);
  if (slide && slide.length) {
    var slideNumber = parseInt(slide[1], 10);
    PresentationStore.setSlide(slideNumber);
  }
}

PresentationStore.addChangeListener(changeset => {
  if (changeset.slide) {
    window.location.hash = "#" + changeset.slide;
  }
});

if (window.location.hash) {
  onHashChange();
}

window.addEventListener("hashchange", onHashChange, false);

React.render(
  <Presentation />,
  document.getElementById("presentation")
);

window.Perf = React.addons.Perf;